package br.com.drogaria.test;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.Ignore;
import org.junit.Test;

import br.com.drogaria.dao.ProdutoDAO;
import br.com.drogaria.domain.Fabricante;
import br.com.drogaria.domain.Produto;

public class ProdutoDAOTeste {
	@Test
	@Ignore
	public void salvar() throws SQLException{
		Produto p = new Produto();
		p.setDescricao("LORATADIN 10 MG");
		p.setPreco(7.95);
		p.setQuantidade(12L);
		
		Fabricante f = new Fabricante();
		f.setCodigo(3L);
		
		p.setFabricante(f);
		
		ProdutoDAO dao = new ProdutoDAO();
		dao.salvar(p);
	}
	
	@Test
	@Ignore
	public void listar() throws SQLException{
		ProdutoDAO dao = new ProdutoDAO();
		ArrayList<Produto> lista = dao.listar();
		
		for(Produto p : lista) {
			System.out.println("C�digo: " + p.getCodigo());
			System.out.println("Descri��o: " + p.getDescricao());
			System.out.println("Preco: " + p.getPreco());
			System.out.println("Quantidade: " + p.getQuantidade());
			System.out.println("C�digo Fab: " + p.getFabricante().getCodigo());
			System.out.println("Descri��o Fab: " + p.getFabricante().getDescricao());
			System.out.println();
		}
	}
	
	@Test
	@Ignore
	public void excluir() throws SQLException{
		Produto p = new Produto();
		p.setCodigo(1L);
		
		ProdutoDAO dao = new ProdutoDAO();
		dao.excluir(p);
	}
	
	@Test
	public void editar() throws SQLException{
		Produto p = new Produto();
		p.setCodigo(2L);
		p.setDescricao("CIMEGRIPE 4 MG");
		p.setPreco(3.50);
		p.setQuantidade(24L);
		Fabricante f = new Fabricante();
		f.setCodigo(5L);
		p.setFabricante(f);
		
		ProdutoDAO dao = new ProdutoDAO();
		dao.editar(p);
		
	}
	
}
